package edu.example.resttemplate.resttemplate.controller;

;
import edu.example.resttemplate.resttemplate.model.User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class CatalogUserController {
    @GetMapping("/catalog/{age}")
    public List<User> getUserFromProducer() {
        List<User> users = List.of(new User("Ivan" ,10L, 100L), new User("Kolya", 20L, 200L));
        RestTemplate restTemplate = new RestTemplate();
        return users.stream().map(user -> restTemplate.getForObject("http://localhost:8081/kafka/create/"+user.getAge(), User.class))
                .collect(Collectors.toList());
    }
}
