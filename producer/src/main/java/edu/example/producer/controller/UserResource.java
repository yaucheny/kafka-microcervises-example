package edu.example.producer.controller;

import edu.example.producer.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("kafka")
@RequiredArgsConstructor
public class UserResource {

    private final KafkaTemplate<String, User> kafkaTemplate;
    private static final String TOPIC = "demo-topic1";

    @GetMapping("/publish/{number}")
    public String post(@PathVariable("number") final Long number) {
        for (long i = 0; i <= number; i++) {
            long k = 10;
            kafkaTemplate.send(TOPIC, new User(String.format("ivan%s", i), i, k * i * 10));
        }
        return "Published successfully";
    }

    @GetMapping("/create/{age}")
    public User getByAge(@PathVariable ("age") int age) {
        List<User> users = new ArrayList<>();
        for (long i = 0; i <= 100; i++) {
            long k = 10;
            users.add(new User(String.format("ivan%s", i), i, k * i * 10));
        }
        return users.get(age);
    }
}
