package edu.example.consumer.listaner;

import edu.example.consumer.model.UserDto;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumer {

    @KafkaListener(topics = "demo-topic1", groupId = "group_json",
            containerFactory = "userKafkaListenerFactory")
    public void consumeJson(UserDto userDto) {
        System.out.println("Consumed JSON Message: " + userDto);
    }
}
